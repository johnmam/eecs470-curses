/**
 * In order to use redeye, verilog programs have to generate log files.  This
 * c library provides hooks that allow testbenches to generate the necessary 
 * log files.
 */

/**
 * functions to add:
 *    - disassemble instruction to get string.
 *    - box with list of values
 *    - getchar? 
 * 
 * TODOs:
 *    - need to mask bval when testing head/tail pointer
 *    - vars in table shit their pants when they have an x
 *    - roll "get var from handle" pattern into helper function
 *    - add string support to box function / add > 32-bit support
 *    - center title on drawbox
 *    - improve aestetics with ────  (196?)   │ │   (graphical mode )
 *
 * corner cases to test:
 *    x column where value exceeds width
 *    x column where widht exceeds value
 *    x vars with x's
 *
 *    - box title with biiiig width
 *    
 * bugs:
 *    - having a column heading with 2 or more newlines screws stuff up.
 *    - vpi leaks ALL OVER THE PLACE
 *    - c memory leaks ALL OVER THE PLACE
 */

#include <stdio.h>
#include <unistd.h>

#define _GNU_SOURCE
#include <string.h>
#undef _GNU_SOURCE

#include <stdlib.h>
#include <stdarg.h>
#include <time.h>
#include <stdint.h>

#include "sv_vpi_user.h"

////////////
// internal types
////////////
#define NUM_TABLE_STRINGS 128
#define STRING_LENGTH     1024

/////////////////////
// static global vars
/////////////////////

//////////////
// function declarations
//////////////
//functions called by the testbench
PLI_INT32 easydraw_clock_box(PLI_BYTE8*);
PLI_INT32 easydraw_value_box(PLI_BYTE8*);
PLI_INT32 print_selective_struct_table(PLI_BYTE8*);

//internal helper functions
void print_column_headers(char**, int, char**, int*, int, int*, int*);
int get_string_width(char*, int);
int get_table_width(char**, int);
int get_packed_ranges(vpiHandle, int*);
void print_row(const char*, char**, int, int, char**, int*, int);
void repeat_char(int, int);

/**
 * Can't use gdb properly with the verilog vpi, so we insert this function as a
 * breakpoint.
 */
volatile int breaker()
{
    return 1;
}

/**
 * This function just draws a diagram showing the current edge of the clock
 * at a given x, y position in the terminal.
 * 
 * The only arguments are a terminal x position, y position, and clock state.
 * $clock_box() also accepts an optional 4th integer argument: the clock count.
 * 
 * Example call:
 * input:
 *    $clock_box(50, 0, clock, clock_count);
 * output:
 *       ___
 *   ___|     clock cycle 00032
 */ 
PLI_INT32 easydraw_clock_box(PLI_BYTE8* user)
{
    //start out by reading position vars
    int termx, termy;
    
    vpiHandle systf_handle, arg_iter;
    systf_handle = vpi_handle(vpiSysTfCall, NULL);
    arg_iter = vpi_iterate(vpiArgument, systf_handle);
    if(arg_iter == NULL)
    {
        vpi_printf("error in print_var\n");
        return 0;
    }

    //read in the x and y coords
    vpiHandle termx_handle, termy_handle;
    termx_handle = vpi_scan(arg_iter);
    termy_handle = vpi_scan(arg_iter);
    s_vpi_value read_xy_args;
    read_xy_args.format = vpiIntVal;
    vpi_get_value(termx_handle, &read_xy_args);
    termx = read_xy_args.value.integer;
    read_xy_args.format = vpiIntVal;
    vpi_get_value(termy_handle, &read_xy_args);
    termy = read_xy_args.value.integer;
    
    //read in the clock state & if necessary the number of clock cycles
    int clock_state;
    vpiHandle arghandle;
    
    s_vpi_value read_clock_args;
    arghandle = vpi_scan(arg_iter);
    read_clock_args.format = vpiVectorVal;
    vpi_get_value(arghandle, &read_clock_args);
    if(read_clock_args.value.vector[0].bval & 0x01)
    {
        clock_state = -1;
    }
    else
    {
        clock_state = read_clock_args.value.vector[0].aval & 0x01;
    }
    
    //char* clock_count = NULL;
    int clock_count = -1;
    if((arghandle = vpi_scan(arg_iter)) != NULL)
    {
        read_clock_args.format = vpiIntVal;
        vpi_get_value(arghandle, &read_clock_args);
        
        //clock_count = malloc(200);
        //strncpy(clock_count, read_clock_args.value.str, 100);
        clock_count = read_clock_args.value.integer;
    }
    
    //do the drawing
    switch(clock_state) 
    {
        case 1:
        {
            vpi_printf("\033[%i;%iH__|", termy + 1, termx);
            vpi_printf("\033[%i;%iH__", termy, termx + 3);
            break;
        }
        
        case 0:
        {
            vpi_printf("\033[%i;%iH|__", termy + 1, termx + 2);
            vpi_printf("\033[%i;%iH__", termy, termx);
            break;
        }

        default:
        {
            vpi_printf("\033[%i;%iHxxxxx", termy, termx);
            break;
        }
    }
    
    //print the clock count, if applicable
    if(clock_count != -1)
    {
        vpi_printf("\033[%i;%iHclock cycle %05i", termy + 1, termx + 7, clock_count);
    }
    
    return 0;
}

/**
 * termx, termy, tablewidth, tableheight, title, {var, str}, ....
 * $easydraw_value_box(int, int, int, int, str, var, str, var, str...)
 *
 * the blood of any string you clip off is on your own hands... make sure the
 * tablewidth is big enough.
 */
PLI_INT32 easydraw_value_box(PLI_BYTE8* user)
{
    //vars
    int termx, termy, tabwidth, tabheight;
    
    //set up the name tables
    char** name_table = malloc(NUM_TABLE_STRINGS * sizeof(char*));
    for(int i = 0; i < NUM_TABLE_STRINGS; i++)
    {
        name_table[i] = malloc(STRING_LENGTH * sizeof(char));
    }
    int* value_table = malloc(NUM_TABLE_STRINGS);
    
    
    //initialize the list to read the args
    vpiHandle systf_handle, arg_iter;
    systf_handle = vpi_handle(vpiSysTfCall, NULL);
    arg_iter = vpi_iterate(vpiArgument, systf_handle);
    if(arg_iter == NULL)
    {
        vpi_printf("error in $easydraw_value_box\n");
        return 0;
    }

    //read in the x and y coords
    vpiHandle termx_handle, termy_handle, tabwidth_handle, tabheight_handle;
    termx_handle = vpi_scan(arg_iter);
    termy_handle = vpi_scan(arg_iter);
    tabwidth_handle = vpi_scan(arg_iter);
    tabheight_handle = vpi_scan(arg_iter);
    
    //very repetetive
    s_vpi_value read_beginning_args;
    read_beginning_args.format = vpiIntVal;
    vpi_get_value(termx_handle, &read_beginning_args);
    termx = read_beginning_args.value.integer;
    
    read_beginning_args.format = vpiIntVal;
    vpi_get_value(termy_handle, &read_beginning_args);
    termy = read_beginning_args.value.integer;
    
    read_beginning_args.format = vpiIntVal;
    vpi_get_value(tabwidth_handle, &read_beginning_args);
    tabwidth = read_beginning_args.value.integer;
    
    read_beginning_args.format = vpiIntVal;
    vpi_get_value(tabheight_handle, &read_beginning_args);
    tabheight = read_beginning_args.value.integer;
    
    //read in title
    vpiHandle arg;
    char* title = malloc(STRING_LENGTH);
    s_vpi_value read_arg;
    read_arg.format = vpiStringVal;
    arg = vpi_scan(arg_iter);
    vpi_get_value(arg, &read_arg);
    strncpy(title, read_arg.value.str, STRING_LENGTH);
    
    //read in {value, string} pairs
    int index_table_i = 0;
    while((arg = vpi_scan(arg_iter)) != NULL)
    {
        s_vpi_value read_arg;
        read_arg.format = vpiVectorVal;
        vpi_get_value(arg, &read_arg);
        if(read_arg.value.vector[0].bval)
        {
            value_table[index_table_i] = -1;
        }
        else
        {
            value_table[index_table_i] = read_arg.value.vector[0].aval;
        }
        
        arg = vpi_scan(arg_iter);
        read_arg.format = vpiStringVal;
        vpi_get_value(arg, &read_arg);
        strncpy(name_table[index_table_i], read_arg.value.str,
                STRING_LENGTH);
        index_table_i++;
    }
    
    //draw the box header
    vpi_printf("\033[%i;%iH", termy, termx);
    if(strnlen(title, STRING_LENGTH) > (tabwidth - 2))
    {
        title[tabwidth-1] = '\0';
    }
    vpi_printf("-%s", title);
    repeat_char('-', tabwidth - 1 - strnlen(title, STRING_LENGTH));
    termy++;
    
    //print each line of the thing.
    char* rowspace = malloc(tabwidth + 1);
    char* valstring = malloc(STRING_LENGTH);
    for(int i = 0; i < index_table_i; i++)
    {
        memset(rowspace, ' ', tabwidth - 1);
        memcpy(&rowspace[1], name_table[i], strnlen(name_table[i], STRING_LENGTH));
        int valstart = tabwidth - 2 - 8;
        sprintf(valstring, "%08x", value_table[i]);
        memcpy(&rowspace[valstart], valstring, strnlen(valstring, 24));
        
        //ends and null terminator
        rowspace[0] = '|';
        rowspace[tabwidth-1] = '|';
        rowspace[tabwidth] = '\0';
        
        //print
        vpi_printf("\033[%i;%iH", termy, termx);
        vpi_printf("%s", rowspace);
        
        termy++;
    }
    
    //print footer
    vpi_printf("\033[%i;%iH", termy, termx);
    repeat_char('-', tabwidth);
    termy++;
    
    //free everything
    free(rowspace);
    free(valstring);
    for(int i = 0; i < NUM_TABLE_STRINGS; i++)
    {
        free(name_table[i]);
    }
    free(value_table);
    
    return 0;
}

/**
 * This function is for printing an ascii n-curses style table.
 * 
 * int, int          <---- these are the x, y coords to display the table at
 * int, str, ...., int, str    <---- head & tail pointers
 * str               <---- name of struct to read.
 * str, str          <---- name of struct member for column 1 + column name
 * .....             
 * str, str          <---- name of struct member for column n
 *
 * caveats:
 *     - This function's behaviour is undefined if you give it a variable name
 *       that doesn't actually exist.
 *     - You can't use a constant as a "pointer" parameter.  for instance, the 
 *       following call would be invalid.
 *           $easydraw_struct_array_table(0, 0, 6'h10, "ptr", "structs",....);
 *   
 */
PLI_INT32 easydraw_struct_array_table(PLI_BYTE8* user)
{
    //vars
    int termx, termy;
    
    //set up the name tables
    char** name_table = malloc(NUM_TABLE_STRINGS * sizeof(char*));
    for(int i = 0; i < NUM_TABLE_STRINGS; i++)
    {
        name_table[i] = malloc(STRING_LENGTH * sizeof(char));
    }
    
    //set up head and tail tables
    char** index_pointer_table = malloc(NUM_TABLE_STRINGS * sizeof(char*));
    for(int i = 0; i < NUM_TABLE_STRINGS; i++)
    {
        index_pointer_table[i] = malloc(STRING_LENGTH * sizeof(char));
    }
    int* index_val_table = malloc(NUM_TABLE_STRINGS * sizeof(int*));
    
    char* source_struct = malloc(STRING_LENGTH * sizeof(char));
    
    //initialize the list to read the args
    vpiHandle systf_handle, arg_iter;
    systf_handle = vpi_handle(vpiSysTfCall, NULL);
    arg_iter = vpi_iterate(vpiArgument, systf_handle);
    if(arg_iter == NULL)
    {
        vpi_printf("error in print_var\n");
        return 0;
    }

    //read in the x and y coords
    vpiHandle termx_handle, termy_handle;
    termx_handle = vpi_scan(arg_iter);
    termy_handle = vpi_scan(arg_iter);
    s_vpi_value read_xy_args;
    read_xy_args.format = vpiIntVal;
    vpi_get_value(termx_handle, &read_xy_args);
    termx = read_xy_args.value.integer;
    read_xy_args.format = vpiIntVal;
    vpi_get_value(termy_handle, &read_xy_args);
    termy = read_xy_args.value.integer;
    
    //read in head & tail pointer args
    vpiHandle arg;
    int index_table_i = 0;
    while(1)
    {
        //read in the next arg and check if its a string.  If it is, we have
        //read in all of the head and tail pointers.
        //printf("itertation %i\n", index_table_i);
        if(arg_iter == NULL)
        {
            break;
        }
        arg = vpi_scan(arg_iter);
        if(arg == NULL)
        {
            break;
        }
        //vpi_printf("type = %i\n", vpi_get(vpiType, arg));
        if(vpi_get(vpiType, arg) == vpiConstant)
        {
            break;
        }
        //otherwise, the current arg is an index pointer value, so we need to 
        //read it in as well as the following argument, which is an index 
        //pointer name.
        else
        {
            s_vpi_value read_arg;
            read_arg.format = vpiVectorVal;
            vpi_get_value(arg, &read_arg);
            if(read_arg.value.vector[0].bval)
            {
                index_val_table[index_table_i] = -1;
            }
            else
            {
                index_val_table[index_table_i] = read_arg.value.vector[0].aval;
            }
            arg = vpi_scan(arg_iter);
            read_arg.format = vpiStringVal;
            vpi_get_value(arg, &read_arg);
            strncpy(index_pointer_table[index_table_i], read_arg.value.str,
                    STRING_LENGTH);
        }
        index_table_i++;
    }
    
    //read in the struct name
    s_vpi_value read_struct_arg;
    read_struct_arg.format = vpiStringVal;
    vpi_get_value(arg, &read_struct_arg);
    strncpy(source_struct, read_struct_arg.value.str, STRING_LENGTH);
    
    //read in all of the following pairs
    int name_table_i = 0;
    while((arg = vpi_scan(arg_iter)) != NULL)
    {
        s_vpi_value name_table_value;
        name_table_value.format = vpiStringVal;
        vpi_get_value(arg, &name_table_value);
        strncpy(name_table[name_table_i++], name_table_value.value.str, 
                STRING_LENGTH);
    }
    
    //print the name of the struct at the top of the table
    vpi_printf("\033[%i;%iH", termy, termx);
    char* clipper = strchr(source_struct, '.');
    if(++clipper)
    {
        vpi_printf("%s", clipper);
    }
    else
    {
        vpi_printf("err: name not found");
    }
    termy++;
    
    //print the column headers.  roll-it-yourself memset (ick)
    vpi_printf("\033[%i;%iH", termy, termx);
    char* dashes = malloc(get_table_width(name_table, name_table_i) + 5);
    for(int i = 0; i < get_table_width(name_table, name_table_i); i++)
    {
        dashes[i] = '-';
    }
    dashes[get_table_width(name_table, name_table_i)] = '\0';
    vpi_printf("%s", dashes);
    termy++;
    
    vpi_printf("\033[%i;%iH", termy, termx);
    print_column_headers(name_table, name_table_i, index_pointer_table,
                         index_val_table, index_table_i, &termx, &termy);
    
    vpi_printf("\033[%i;%iH", termy, termx);
    vpi_printf("%s", dashes);
    termy++;
    
    //get the depth of the array
    int* ranges = malloc(8 * sizeof(int));
    //TODO: 0 ---> scope???
    vpiHandle base_handle = vpi_handle_by_name(source_struct, 0);
    
    get_packed_ranges(base_handle, ranges);
    
    //for each range, print the values.
    for(int i = 0; i < (ranges[0] + 1); i++)
    {
        vpi_printf("\033[%i;%iH", termy, termx);
        print_row(source_struct, name_table, name_table_i, i,
                  index_pointer_table, index_val_table, index_table_i);
        termy++;
    }
    
    //TODO: free everything.  we actually can't have memory leaks in this code 
    //because it is called all the time.
    for(int i = 0; i < NUM_TABLE_STRINGS; i++)
    {
        free(name_table[i]);
        free(index_pointer_table[i]);
    }
    free(name_table);
    free(index_pointer_table);
    free(source_struct);
    free(index_val_table);
}

/**
 * prints headers for columns.  May modify termx and termy.
 */
void print_column_headers(char** name_table, int num_entries,
                          char** index_name_table, int* index_val_table,
                          int num_index_vals, int* termx, int* termy)
{
    //count height (just the max number of \n in any string)
    int maxheight = 1;
    for(int i = 1; i < num_entries; i += 2)
    {
        int thisheight = 1;
        
        //count height
        for(int j = 0; name_table[i][j]; j++)
        {
            if(name_table[i][j] == '\n')
            {
                thisheight++;
            }
        }
        
        if(thisheight > maxheight)
        {
            maxheight = thisheight;
        }
    }
    
    //iterate over all loops "height" times
    for(int i = 0; i < maxheight; i++)
    {
        vpi_printf("\033[%i;%iH", *termy, *termx);
        
        //draw "index" string
        //wow this is gross.
        if(i == 0)
        {
            vpi_printf("index |");
        }
        else
        {
            vpi_printf("      |");
        }
        
        //draw all of the other strings
        for(int j = 1; j < num_entries; j += 2)
        {
            //if we are in the ith row down from the top, we want to emulate
            //that newline
            //count newlines (actually \0 from above)
            char* str_start = name_table[j];
            for(int k = 0; (k < i) && (str_start != NULL); k++)
            {
                str_start = strchr(str_start, '\n');
            }
            
            //if it's null, just print some spaces.
            if(str_start == NULL)
            {
                int thiswidth = get_string_width(name_table[j], STRING_LENGTH);
                vpi_printf(" ");
                for(int q = 0; q < thiswidth; q++)
                {
                    vpi_printf(" ");
                }
            }
            else
            {   
                //get string width before \n -> \0 shenanigans
                int str_width = get_string_width(name_table[j], STRING_LENGTH);
                
                //first we need to convert the newline following str_start to 
                //a \0 to terminate the string
                if(*str_start == '\n')
                {
                    str_start++;
                }
                char* null_term = strchr(str_start, '\n');
                if(null_term)
                {
                    *null_term = '\0';
                }
                
                vpi_printf(" %s", str_start);
                
                //print padding spaces
                int pad = str_width - strnlen(str_start, STRING_LENGTH);
                for(int q = 0; q < pad; q++)
                {
                    vpi_printf(" ");
                }
                
                //change the null termainator back to a \n
                if(null_term)
                {
                    *null_term = '\n';
                }
            }
                            
            if(j < (num_entries))
            {
                vpi_printf(" |");
            }
        }
        
        //if we are on the first row, print one set of xxxxx for each invalid
        //pointer that we find
        if(i == 0)
        {
            int startedflag = 0;
            for(int i = 0; i < num_index_vals; i++)
            {
                if(index_val_table[i] == -1)
                {
                    if(!startedflag)
                    {
                        startedflag = 1;
                        vpi_printf("<-");
                    }
                    vpi_printf("%s ", index_name_table[i]);
                }
            }
        }
        
        //increment termy
        *termy = *termy + 1;
    }
}

/**
 * easydraw_struct_array_table() prints the following column headers:
 *
 *   index | name_table[1] | name_table[3] | .... | name_table[2n + 1]
 *
 * it is useful for us to know the total width of these headers.
 */
int get_table_width(char** name_table, int num_entries)
{
    int total = 7;   //start out at 7 because first col is "index |"
    for(int i = 1; i < num_entries; i += 2)
    {
        total += get_string_width(name_table[i], STRING_LENGTH);
        total += 3;
    }
    
    return total;
}


/**
 * gets the width that it takes to display a string with newlines in it.
 */
int get_string_width(char* str, int max)
{
    int maxtotal = 0;
    int total = 0;
    for(; *str; str++)
    {
        if((*str == '\n'))
        {
            if(total > maxtotal)
            {
                maxtotal = total;
            }
                    
            total = 0;
        }
        else
        {
            total++;
        }
    }
    
    if(total < maxtotal)
    {        
        return maxtotal;
    }
    else
    {
        return total;
    }
}


/**
 * dest should point to at least 4 ints worth of space.
 */ 
int get_packed_ranges(vpiHandle var_handle, int* dest)
{
    //count number of array elements & get handle to one of the elements.
    vpiHandle element_handle = NULL;
    vpiHandle temp_handle;
    int dims = 0;
    vpiHandle range = vpi_iterate(vpiVarSelect, var_handle);
    while(temp_handle = vpi_scan(range))
    {
        dims++;
        element_handle = temp_handle;
    }
    if(dims)
    {
        dims--;
    }
    
    if(dims > 0)
    {
        memcpy(dest, (int []){dims, 0, vpi_get(vpiSize, element_handle), 0}, 
               4 * sizeof(int));

    }
    else
    {
        memcpy(dest, (int []){vpi_get(vpiSize, var_handle) - 1, 0}, sizeof(int) * 2);
    }
    
    return 0;
}

void print_row(const char* source_struct, char** name_table, int tab_width, int array_ix,
               char** index_name_table, int* index_val_table, int ix_tab_width)
{
    //print index
    vpi_printf("%05d |", array_ix);
    char* varname = malloc(2 * STRING_LENGTH * sizeof(char));
    char* valstr = malloc(STRING_LENGTH);
    for(int i = 0; i < tab_width; i += 2)
    {
        //get handle to value of interest
        sprintf(varname, "%s[%i].%s", source_struct, array_ix, name_table[i]);
        //vpiHandle val_hand = vpi_handle_by_name(varname, 0);
        vpiHandle val_hand = vpi_handle_by_name(varname, 0);
      
        //get value string for <source_struct>[ix].name_table[i]
        s_vpi_value str_reader;
        str_reader.format = vpiHexStrVal;
        vpi_get_value(val_hand, &str_reader);
        strncpy(valstr, str_reader.value.str, STRING_LENGTH);
        
        //truncate the value string if needed
        int colsize = get_string_width(name_table[i + 1], STRING_LENGTH);
        if(strnlen(valstr, STRING_LENGTH) > colsize)
        {
            //cut off the first few chars
            int cutsize = strnlen(valstr, STRING_LENGTH) - colsize;
            memmove(valstr, valstr + cutsize, (colsize + 1));
        }
        else if(strnlen(valstr, STRING_LENGTH) < colsize)
        {
            //add padding at start
            char padder[STRING_LENGTH];
            int padsize = colsize - strnlen(valstr, STRING_LENGTH);
            for(int i = 0; i < padsize; i++)
            {
                padder[i] = ' ';
            }
            padder[padsize] = '\0';
            vpi_printf("%s", padder);
        }
        
        //print the value
        vpi_printf(" %s |", valstr);
    }
    
    //check to see if any of the index vars match up
    int flag = 0;
    int num_ptrs = 0;
    for(int i = 0; i < ix_tab_width; i++)
    {
        if(index_val_table[i] == array_ix)
        {
            num_ptrs++;
        }
    }
    
    int writecount = 0;
    for(int i = 0; i < ix_tab_width; i++)
    {
        if(index_val_table[i] == array_ix)
        {
            writecount++;
            if(!flag)
            {
                vpi_printf("<-");
                flag = 1;
            }
            vpi_printf("%s", index_name_table[i]);
            if(writecount < num_ptrs)
            {
                vpi_printf(", ");
            }
        }
    }
    
    free(varname);
    free(valstr);
}

/**
 * utility function that prints the given string repeated n times.
 */
void repeat_char(int c, int n)
{
    if(n > 0)
    {
        char* repeat_space = malloc(n + 1);
        memset(repeat_space, c, n);
        repeat_space[n] = '\0';
        vpi_printf("%s", repeat_space);
        free(repeat_space);
    }
}
   
