`timescale 1ns/100ps

`define MY_WIDTH 32

module testbench;
    //somewhat necessary clock
    logic clock;
    logic foobar;
    
    //variables used in the testbench.
    logic [`MY_WIDTH-1:0] A;
    logic [`MY_WIDTH-1:0] B;
    logic [`MY_WIDTH-1:0] Sum;
    logic Cin;
    logic Cout;

    //DUTs
    adder #(`MY_WIDTH) myadd(.A(A), .B(B), .Sum(Sum), .Cin(Cin), .Cout(Cout));
    adder otheradder(.A(10), .B(24), .Sum(), .Cin(1), .Cout());
    
    initial begin
        //initialize vars to devault
        clock = 0;
        A = '0;
        B = '0;
        Cin = '0;
        
        //Stimulus.
        @(negedge clock);
        #1;
        A = 32'h10;
        B = 32'h11;
        @(negedge clock);
        #1;
        Cin = 1'h1;
        @(negedge clock);
        #1;
        A = 32'h27;
        B = 32'h17;
        Cin = 1'h0;
        @(negedge clock);
        #1;
        A = 32'hff;
        B = 32'hff;
        Cin = 1'h1;
        @(negedge clock);
        $finish;
    end
    
    //System clock (yes, I know its not necessary, but it makes testing easier
    //because it allows us to do @(negedge clock) in the stimulus section.
    always begin
        #10;
        clock = ~clock;
        
        //remember to call logging function
        
    end

    //drawing function
    string user_block;
    always @(clock) begin
        $write("\033[2J");
        $write("\033[0;0H");
        
        $easydraw_clock_box(10, 10, clock);
        
        $fscanf(32'h8000_0000, "%c", user_block);
        if(user_block == "q") begin
            $finish;
        end

    end
        
endmodule // testbench
