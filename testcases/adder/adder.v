`default_nettype none
  
module full_adder(input           A,
                  input           B,
                  input           Cin,
                  output logic    S,
                  output logic    Cout);
    logic pre_sum = A ^ B;
    logic [1:0] pre_carry;
    assign pre_carry[0] = A & B;
    assign pre_carry[1] = pre_sum & Cin;

    assign S = pre_sum ^ Cin;
    assign Cout = |pre_carry;
endmodule;

module adder
  #(parameter WIDTH = 8)
  (input [WIDTH-1:0]        A,
   input [WIDTH-1:0]        B,
   input                    Cin,
   output logic [WIDTH-1:0] Sum,
   output logic             Cout);

    logic [WIDTH:0] carries;
    assign carries[0] = Cin;
    
    genvar i;
    generate
        for(i = 0; i < WIDTH; i++) begin
            full_adder fa(A[i], B[i], carries[i], Sum[i], carries[i+1]);
        end
    endgenerate
    
    assign Cout = carries[WIDTH-1];
endmodule; // adder

