`timescale 1ns/100ps

`define MY_WIDTH 32

module testbench;
    //somewhat necessary clock
    logic clock;
    logic foobar;
    
    //variables used in the testbench.
    logic [`MY_WIDTH-1:0] A [0:3];
    logic [`MY_WIDTH-1:0] Sum;
    logic Cout;

    //DUTs
    multi_adder #(.WIDTH(`MY_WIDTH), .DEPTH(4))
        myadd(.A(A), .Sum(Sum), .Cout(Cout));
    
    initial begin
        //initialize vars to devault
        clock = 0;
        for(int i = 0; i < 4; i++) begin
            A[i] = 32'h10;
        end

        //start up C logging *only after* all vars have been initialized (so
        //that we get correct initial values)        
        //Stimulus.
        @(negedge clock);
        #1;
        A[2] = 32'h10;
        $display("sum = %d", Sum);
        @(negedge clock);
        #1;
        @(negedge clock);
        #1;
        A[1] = 32'h27;
        @(negedge clock);
        #1;
        A[0] = 32'hff;
        @(negedge clock);
        $finish;
    end
    
    //System clock (yes, I know its not necessary, but it makes testing easier
    //because it allows us to do @(negedge clock) in the stimulus section.
    always begin
        #10;
        clock = ~clock;

        //remember to call logging function
        
    end
endmodule // testbench
