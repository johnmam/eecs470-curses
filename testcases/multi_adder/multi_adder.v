`default_nettype none
  
module full_adder(input           A,
                  input           B,
                  input           Cin,
                  output logic    S,
                  output logic    Cout);
    logic pre_sum;
    logic [1:0] pre_carry;

    always_comb begin
        pre_sum = A ^ B;
        pre_carry[0] = A & B;
        pre_carry[1] = pre_sum & Cin;

        S = pre_sum ^ Cin;
        Cout = pre_carry[0] | pre_carry[1];
    end
    
endmodule;

module adder
  #(parameter WIDTH = 8)
  (input [WIDTH-1:0]        A,
   input [WIDTH-1:0]        B,
   input                    Cin,
   output logic [WIDTH-1:0] Sum,
   output logic             Cout);

    logic [WIDTH:0] carries;
    assign carries[0] = Cin;
    
    genvar i;
    generate
        for(i = 0; i < WIDTH; i++) begin
            full_adder fa(A[i], B[i], carries[i], Sum[i], carries[i+1]);
        end
    endgenerate
    
    assign Cout = carries[WIDTH-1];
endmodule; // adder

module multi_adder
  #(parameter WIDTH = 8,
    parameter DEPTH = 4)
    (input [WIDTH-1:0] A [0:DEPTH-1],
     output logic [WIDTH-1:0] Sum,
     output logic Cout);

    genvar i;
    logic [WIDTH-1:0] results [0:DEPTH-2];
    logic carries [0:DEPTH-2];
    generate
        for(i = 0; i < (DEPTH - 1); i++) begin
            if(i == 0) begin
                adder #(.WIDTH(WIDTH)) a(.A(A[0]), .B(A[1]), .Cin(1'b0), .Sum(results[0]),
                        .Cout(carries[0]));
            end else begin
                adder #(.WIDTH(WIDTH)) a(.A(A[i + 1]), .B(results[i - 1]), .Cin(carries[i - 1]), 
                        .Sum(results[i]), .Cout(carries[i]));
            end
        end
    endgenerate
    
    assign Sum = results[DEPTH-2];
    assign Cout = carries[DEPTH-2];
endmodule
