/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  RS.v                                                //
//                                                                     //
//  Description :  This module contains both memory for the reservation//
//                 stations themselves, as well as logic for           //
//                 dispatching and issuing instructions                //
//                                                                     //
//                 This module takes as input 2 instructions from the  //
//                 decode stage that are to be dispatched.  It decides //
//                 Which reservations stations to put                  //
/////////////////////////////////////////////////////////////////////////

`timescale 1ns/100ps

typedef struct packed {
   //tells whether the current entry is valid or not
   logic       valid;

   logic [1:0] [63:0] ops;
} ringbuf_entry_t;
`define EMPTY_RINGBUF_ENTRY_T {1'b0, 64'h0, 64'h0};

`define NUM_ENTRIES 18
module ringbuf(input               clock,
	       input 		   reset,

	       input 		   operands_ready,
	       input [1:0] [63:0]  operands,

               input 		   gnt,

	       output logic        operands_available,
	       output              space_available,
	       output logic [1:0] [63:0] operands_out
	       );
   logic [$clog2(`NUM_ENTRIES+1)-1:0] head;
   logic [$clog2(`NUM_ENTRIES+1)-1:0] tail;

   ringbuf_entry_t [`NUM_ENTRIES-1:0] entries;

   assign operands_out = entries[head].ops;
   assign operands_available = entries[head].valid;
   assign space_available = !(entries[tail].valid);


   always_ff @(posedge clock) begin
      if(reset) begin
	 head <= #1 'h0;
	 tail <= #1 'h0;
      end

      //advance head
      if(reset) begin
	 head <= #1 'h0;
      end else if(gnt && entries[head].valid) begin
	 if(head == (`NUM_ENTRIES - 1)) begin
	    head <= #1 'h0;
	 end else begin
	    head <= #1 head + 'h1;
	 end
      end

      //advance tail.
      if(reset) begin
	 tail <= #1 'h0;
      end else if(operands_ready && !entries[tail].valid) begin
	 if(tail == (`NUM_ENTRIES - 1)) begin
	    tail <= #1 'h0;
	 end else begin
	    tail <= #1 tail + 'h1;
	 end
      end
   end // always_ff @

   genvar i;
   generate
      for(i = 0; i < `NUM_ENTRIES; i++) begin
	 always_ff @(posedge clock) begin
	    if(reset) begin
	       entries[i] <= #1 `EMPTY_RINGBUF_ENTRY_T;
	    end else if((i == head) && (gnt)) begin
	       entries[i] <= #1 `EMPTY_RINGBUF_ENTRY_T;
	    end else if((i == tail) && (operands_ready)) begin
	       entries[i] <= #1 {1'b1, operands[0], operands[1]};
	    end
	 end
      end // for (i = 0; i < `NUM_ENTRIES; i++)
   endgenerate
endmodule // ringbuf
