`timescale 1ns/100ps

typedef struct packed {
   logic       valid;

   logic [1:0] [63:0] ops;
   logic [63:0]       partial_sum;
   logic [63:0]       accumulator;
} adder_pipeline_t;
`define EMPTY_ADDER_PIPELINE_T {1'b0, 64'h0, 64'h0, 64'h0, 64'h0};

module pipelined_adder(input              clock,
		       input 		  reset,

		       input 		  enable,
		       input [1:0] [63:0] operands,
                       input 		  result_gnt,

		       output 		  available,
		       output             result_ready,
		       output [63:0] 	  result
		       );
   adder_pipeline_t [2:0] pipelines;
   adder_pipeline_t [2:0] next_pipe;

   logic [2:0] stall;

   //stall lines
   assign stall[0] = stall[1] && pipelines[0].valid;
   assign stall[1] = stall[2] && pipelines[1].valid;
   assign stall[2] = pipelines[2].valid && (!result_gnt);

   assign available = !stall[0];

   //too lazy to write a generate statement for this test code.
   //first pipeline register.
   always_ff @(posedge clock) begin
      if(reset) begin
	 pipelines[0] <= #1 `EMPTY_ADDER_PIPELINE_T;
      end else if(enable) begin
	 pipelines[0] <= #1 {1'b1, operands[0], operands[1], 64'h0, 64'h0};
      end else if(!stall[0]) begin
	 pipelines[0] <= #1 `EMPTY_ADDER_PIPELINE_T;
      end
   end

   //second pipeline register
   assign next_pipe[1].partial_sum = {1'b0, pipelines[0].ops[0][31:0]} +
				     {1'b0, pipelines[0].ops[1][31:0]};
   assign next_pipe[1].ops = pipelines[0].ops;
   assign next_pipe[1].valid = pipelines[0].valid;
   assign next_pipe[1].accumulator = 64'h0;


   always_ff @(posedge clock) begin
      if(reset) begin
	 pipelines[1] <= #1 `EMPTY_ADDER_PIPELINE_T;
      end else if(!stall[1]) begin
	 pipelines[1] <= #1 next_pipe[1];
      end
   end

   //final pipeline register
   assign next_pipe[2].partial_sum = {1'b0, pipelines[1].ops[0][63:32]} +
				     {1'b0, pipelines[1].ops[1][63:32]};
   assign next_pipe[2].ops = pipelines[1].ops;
   assign next_pipe[2].valid = pipelines[1].valid;
   assign next_pipe[2].accumulator = pipelines[1].accumulator +
				     pipelines[1].partial_sum;

   always_ff @(posedge clock) begin
      if(reset) begin
	 pipelines[2] <= #1 `EMPTY_ADDER_PIPELINE_T;
      end else if(!stall[2]) begin
	 pipelines[2] <= #1 next_pipe[2];
      end
   end

   assign result = pipelines[2].accumulator + pipelines[2].partial_sum;
   assign result_ready = pipelines[2].valid;
endmodule // pipelined_adder
