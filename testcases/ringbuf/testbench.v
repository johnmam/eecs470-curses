`timescale 1ns/100ps

module testbench;
    logic clock;
    logic reset;
    int 	 clock_count;
    logic [31:0] randomholder;
    
    //ringbuf inputs
    logic ringbuf_input_ready;
    logic [1:0] [63:0] ringbuf_operands;
    logic 	      load_new_operands;

    //ringbuf outputs
    logic 	      ringbuf_operands_available;
    logic              ringbuf_space_available;
    logic [1:0] [63:0] ringbuf_operands_out_adder_in;

    //adder inputs
    logic              result_gnt;

    //adder outputs
    logic              available;
    logic              result_ready;
    logic [63:0]       result;

    //instantiate DUTs
    ringbuf ringbuf_inst(
			 .clock(clock),
			 .reset(reset),
			 .operands_ready(ringbuf_input_ready),
			 .operands(ringbuf_operands),
			 .gnt(load_new_operands),

			 .operands_available(ringbuf_operands_available),
			 .space_available(ringbuf_space_available),
			 .operands_out(ringbuf_operands_out_adder_in)
			 );
    pipelined_adder add_inst(
			     .clock(clock),
			     .reset(reset),

			     .enable(load_new_operands),
			     .operands(ringbuf_operands_out_adder_in),
			     .result_gnt(result_gnt),

			     .available(available),
			     .result_ready(result_ready),
			     .result(result)
			     );
    
    //connection between DUTs.
    assign load_new_operands = available && ringbuf_operands_available;
    
    initial begin
        //initialze variables
        $write("hello\n");
        clock = 1'b0;
        reset = 1'b1;
        ringbuf_input_ready = 1'b0;
        ringbuf_operands = {64'h0, 64'h0};
        result_gnt = 1'b0;
        
        //randomize
        @(negedge clock);
        reset = 1'b0;
        for(int i = 0; i < 10000; i++) begin
	    @(negedge clock);
	    ringbuf_input_ready = $random;
            randomholder= $random;
	    if(ringbuf_input_ready) begin
	        ringbuf_operands[0] = {48'b0, randomholder[15:0]};
	        ringbuf_operands[1] = {48'b0, randomholder[31:16]};
	    end else begin
	        ringbuf_operands[0] = 1'b0;
	        ringbuf_operands[1] = 1'b0;
	    end
	    result_gnt = $random;
        end

        $write("test finished\n");
        $finish;
    end // initial begin

    //flip clock
    always begin
        #10;
        clock = ~clock;
        if(clock) begin
	    clock_count++;
        end
    end

    //print results on clock edge
    string user_input;
    always @(clock) begin
        #2;

        //clear the screen
        $write("\033[2J");
        
        //draw the ring buffer & its inputs
//`ifdef NOT_DEFINED
        $easydraw_clock_box(60, 2, clock, clock_count);
        $easydraw_value_box(1, 5, 10, 0, "reset",
                            reset, "");
        $easydraw_value_box(1, 15, 25, 0, "operand inputs",
			    ringbuf_input_ready, "valid:",
			    ringbuf_operands[0], "operand 0:",
			    ringbuf_operands[1], "operand 1:");
        $easydraw_struct_array_table(30, 4,
				     ringbuf_inst.head, "head",
				     ringbuf_inst.tail, "tail",
				     "testbench.ringbuf_inst.entries",
				     "valid", "valid",
				     "ops[0]", "operand 0",
				     "ops[1]", "operand 1");

        //draw the pipelined adder
        $easydraw_value_box(85, 20, 25, 0, "adder pipeline 0",
			    add_inst.pipelines[0].valid, "valid:",
			    add_inst.pipelines[0].ops[0], "operand 0:",
			    add_inst.pipelines[0].ops[1], "operand 1:",
			    add_inst.pipelines[0].partial_sum, "partial sum:",
			    add_inst.pipelines[0].accumulator, "accumulator:");
        $easydraw_value_box(115, 20, 25, 0, "adder pipeline 1",
			    add_inst.pipelines[1].valid, "valid:",
			    add_inst.pipelines[1].ops[0], "operand 0:",
			    add_inst.pipelines[1].ops[1], "operand 1:",
			    add_inst.pipelines[1].partial_sum, "partial sum:",
			    add_inst.pipelines[1].accumulator, "accumulator:");
        $easydraw_value_box(145, 20, 25, 0, "adder pipeline 2",
			    add_inst.pipelines[2].valid, "valid:",
			    add_inst.pipelines[2].ops[0], "operand 0:",
			    add_inst.pipelines[2].ops[1], "operand 1:",
			    add_inst.pipelines[2].partial_sum, "partial sum:",
			    add_inst.pipelines[2].accumulator, "accumulator:");
//`endif
        //block until user presses enter
        $fscanf(32'h8000_0000, "%c", user_input);
        if(user_input == "q") begin
	    $finish;
        end

    end
endmodule // testbench
