# README #

### What is this repository for? ###

* eecs470-curses gives you a few function calls that make it easy to create an
  in-terminal "gui" like the one that the instructors gave you for project 3!
  In fact, you can replicate the one given in project 3 with only about 15
  function calls.

### How do I get set up? ###

* Summary of set up

To use eecs470-curses, clone this repository and then copy the files under /src/
to a directory in your project.  Also, add the following to your Makefile to
compile these C files:
    
    CURSES_SRC_DIR = <directory_to_src>
    PLIFLAGS = -P ../$(CURSES_SRC_DIR)/eecs470_curses_pli.tab +vpi -cflags "-std=gnu99 -g -Wall -Werror" $(CURSES_SRC_DIR)/easy_draw.c
    VCSFLAGS += PLIFLAGS

* How to run tests
Go to the /tests/ directory and run make, then run ./ring_buffer/simv for a demo.
Press <enter> to advance the clock & <q> <enter> to quit.

### Who do I talk to? ###

* Please email johnmam@umich.edu immediately with any bug reports.  Please
  include a back-trace & make sure you are using the latest version.

Happy hacking :)